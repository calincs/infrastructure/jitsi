#!/bin/sh

kubectl get namespace jitsi || kubectl create namespace jitsi

kubectl create secret generic jitsi-config -n jitsi \
  --from-literal=JICOFO_COMPONENT_SECRET=lestforget \
  --from-literal=JICOFO_AUTH_PASSWORD=lestforget \
  --from-literal=JVB_AUTH_PASSWORD=lestforget

kubectl apply -k  ./lincs-jitsi
