# Jitsi install templates for LINCS using Kustomize

This repo contains templates that will deploy Jitsi on the LINCS production kubernetes cluster using [Kustomize](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/kustomization/). It is based on the Jitsi repo [here](https://github.com/jitsi-contrib/jitsi-kubernetes/tree/main/doc/kustomize).

Ensure that `kubectl` is installed and is pointing to the correct cluster.

## Install

```bash
./install.sh
```

## Uninstall

```bash
./uninstall
```
